# include "MyStack.h"
# include "Coordinate.h"
# include <iostream>
# include <stdlib.h>
using namespace std;
//实现括号匹配，遇到符号先入栈，如果向后遍历有与之匹配的就出栈，
//最后看栈是否为空，若空就匹配，非空则不匹配；
//实现原理：有两个栈，其一是存不匹配的字符，其二是存放所需的字符；
//遍历字符数组，取出第一个字符如果与所需字符不匹配就将其压入栈中，
//所需字符是根据遍历字符数组决定的
//若果所需字符非空还要将其压入存放所需字符的栈中。
int main(void)
{
	MyStack<char> *pStack = new MyStack<char>(50);
	MyStack<char> *pNeed = new MyStack<char>(50);
	char str[] = "[()]]";
	char currentNeed = 0;
	for (int i = 0; i < strlen(str); i++)
	{
		if (str[i] != currentNeed)
		{
			pStack->push(str[i]);
			switch (str[i])
			{
			case '(':
				if (currentNeed != 0)
				{
					pNeed->push(currentNeed);
				}
				currentNeed = ')';
				break;
			case '[':
				if (currentNeed != 0)
				{
					pNeed->push(currentNeed);
				}
				currentNeed = ']';
				break;
			default:
				cout << "字符串括号不匹配" << endl;
				system("pause");
				return 0;
			}
		}
		else
		{
			char elem;
			pStack->pop(elem);
			if (!pNeed->pop(currentNeed))
			{
				currentNeed = 0;
			}
		}

	}
	if (pStack->stackEmpty())
	{
		cout << "字符串括号匹配" << endl;
	}
	else
	{
		cout << "字符串括号不匹配" << endl;
	}
	
	
	
	
	delete pStack;
	delete pNeed;
	pStack = NULL;
	pNeed = NULL;
	return 0;
}