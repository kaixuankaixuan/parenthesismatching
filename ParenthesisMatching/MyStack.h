# ifndef MYSTACK_H
# define MYSTACK_H
# include "Coordinate.h"
# include <iostream>
# include <stdlib.h>
using namespace std;
template <typename T>
class MyStack
{
public:
	MyStack(int size);
	~MyStack();
	bool stackEmpty();
	bool stackFull();
	void clearStack();
	int stackLengh();
	void push(T elem);
	bool pop(T &elem);
	void stackTraverse(bool isFromButtom);
private:
	T *m_pBuffer;
	int m_iSize;
	int m_iTop;
};
template <typename T>
MyStack<T>::MyStack(int size)
{
	m_iSize = size;
	m_pBuffer = new T[size];	//因为是栈，最开始就申请了内存的大小了。
	m_iTop = 0;
}
template <typename T>
MyStack<T>::~MyStack()
{
	delete[]m_pBuffer;//删除的是数字，[]
	m_pBuffer = NULL;

}
template <typename T>
bool MyStack<T>::stackEmpty()
{
	if (m_iTop == 0)
		return true;
	return false;
}
template <typename T>
bool MyStack<T>::stackFull()
{
	if (m_iTop == m_iSize)
		return true;
	return false;
}
template <typename T>
void MyStack<T>::clearStack()
{
	m_iTop = 0;//只要头指的是0，不管这块内存有没有值都是无效的；
}
template <typename T>
int MyStack<T>::stackLengh()
{
	return m_iTop;
}
template <typename T>
void MyStack<T>::push(T elem)
{
	if (!stackFull())
	{
		m_pBuffer[m_iTop] = elem;
		m_iTop++;
	}



}
template <typename T>
bool MyStack<T>::pop(T &elem)
{
	if (stackEmpty())
		return false;
	m_iTop--;
	elem = m_pBuffer[m_iTop];
	return true;
}
template <typename T>
void MyStack<T>::stackTraverse(bool isFromButtom)
{
	if (isFromButtom)
	{
		for (int i = 0; i < m_iTop; i++)
		{
			cout << m_pBuffer[i];
			//m_pBuffer[i].printCoordinate();
		}
	}
	else
	{
		for (int i = m_iTop - 1; i >= 0; i--)
		{
			cout << m_pBuffer[i];
			//m_pBuffer[i].printCoordinate();
		}
	}

}

# endif