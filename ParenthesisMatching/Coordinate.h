#ifndef COORDINATE_H
#define COORDINATE_H
# include <iostream>
using namespace std;
class Coordinate
{
public:
	Coordinate(int x=0, int y=0);
	void printCoordinate();
	friend ostream &operator <<(ostream &out, Coordinate &coordinate);
private:
	int m_iX;
	int m_iY;
};
#endif;