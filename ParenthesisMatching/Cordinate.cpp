# include "Coordinate.h"
# include <stdlib.h>
# include <iostream>
using namespace std;
ostream &operator <<(ostream &out, Coordinate &coordinate)
{
	out << "(" << coordinate.m_iX << "," << coordinate.m_iY << ")" << endl;
	return out;
}
Coordinate::Coordinate(int x, int y)
{
	m_iX = x;
	m_iY = y;
}
void Coordinate::printCoordinate()
{
	cout << "(" << m_iX << "," << m_iY << ")" << endl;
}